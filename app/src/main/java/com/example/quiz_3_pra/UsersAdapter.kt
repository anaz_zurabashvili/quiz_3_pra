package com.example.quiz_3_pra

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView


class UsersAdapter(
    private var users: MutableList<User>,
    private val listener: OnUserClickListener
) :
    RecyclerView.Adapter<UsersAdapter.UsersViewHolder>() {

    class UsersViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(user: User) {
            itemView.findViewById<TextView>(R.id.tvFirstName).text = user.firstName
            itemView.findViewById<TextView>(R.id.tvLastName).text = user.lastName
            itemView.findViewById<TextView>(R.id.tvEmailAddress).text = user.emailAddress
//            itemView.findViewById<ImageView>(R.id.ivUpdate).setOnClickListener {
////            user.changeInfo("ana", "zurabashvili",
////                "anaz.zurabashvili@gmail.com")
//                val intent = Intent(itemView.context, UserActivity::class.java)
//                intent.putExtra("user", user)
//                intent.putExtra("user-p", position)
//                itemView.context.startActivity(intent)
////            notifyItemChanged(position)
//            }
//            listener.onUpdateClick(itemView.findViewById<ImageView>(R.id.ivUpdate), adapterPosition)
        }
    }

    interface OnUserClickListener {
        fun onUpdateClick(user: User, position: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UsersViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.item_user, parent, false)

        return UsersViewHolder(view)
    }

    override fun onBindViewHolder(holder: UsersViewHolder, position: Int) {
        val user = users[position]
        holder.bind(user)
        holder.itemView.findViewById<ImageView>(R.id.ivDelete).setOnClickListener {
            this.users.removeAt(position)
            notifyItemRemoved(position)
        }
        holder.itemView.findViewById<ImageView>(R.id.ivUpdate).setOnClickListener {
            listener.onUpdateClick(user, position)
        }
//        holder.itemView.findViewById<ImageView>(R.id.ivUpdate).setOnClickListener {
////            user.changeInfo("ana", "zurabashvili",
////                "anaz.zurabashvili@gmail.com")
//            val intent = Intent(holder.itemView.context, UserActivity::class.java)
//            intent.putExtra("user", user)
//            intent.putExtra("user-p", position)
//            holder.itemView.context.startActivity(intent)
////            notifyItemChanged(position)
//        }
    }


    override fun getItemCount(): Int = users.size

}
