package com.example.quiz_3_pra

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.view.View
import android.widget.Button
import androidx.activity.result.contract.ActivityResultContracts
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.quiz_3_pra.UsersAdapter.OnUserClickListener

class UsersActivity : AppCompatActivity(), OnUserClickListener {
    val users = mutableListOf<User>()
    private lateinit var btnAdd: Button
    private lateinit var recyclerView: RecyclerView
    private lateinit var adapter: UsersAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_users)
        init()
    }

    private fun init() {
        setData()
        initRecyclerView()
    }

    private fun initRecyclerView() {
        recyclerView = findViewById(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(this)
        adapter = UsersAdapter(users, this)
        recyclerView.adapter = adapter

        btnAdd = findViewById(R.id.btnAdd)
        btnAdd.setOnClickListener(btnAddClick)
    }

    private val btnAddClick = View.OnClickListener {
        val intent = Intent(this, UserActivity::class.java)
        startForResult.launch(intent)
    }

    private val startForResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode == RESULT_OK) {
                d("daemata", "daemata")
                var user = it.data?.extras?.get("user") as User
                users.add(user)
                adapter.notifyDataSetChanged()
                intent.removeExtra("user")
            }
        }

    override fun onUpdateClick(user: User, position: Int) {
        val intent = Intent(this, UserActivity::class.java)
        intent.putExtra("user", user)
        intent.putExtra("position", position)
        editStartForResult.launch(intent)
    }

    private val editStartForResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode == RESULT_OK) {
                d("sheicvala", "sheicvala")
                val position = it.data?.extras?.getInt("position")
                users[position!!] = it.data?.extras?.get("user") as User
                adapter.notifyItemChanged(position!!)
                intent.removeExtra("user")
                intent.removeExtra("position")

            }
        }

    private fun setData() {
        users.add(User("User 1", "Content 1", "emailAddress"))
        users.add(User("User 2", "Content 2", "emailAddress"))
        users.add(User("User 3", "Content 3", "emailAddress"))
        users.add(User("User 4", "Content 4", "emailAddress"))
        users.add(User("User 5", "Content 5", "emailAddress"))
        users.add(User("User 6", "Content 6", "emailAddress"))
    }

}