package com.example.quiz_3_pra

import android.os.Bundle
import android.util.Log.d
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity

class UserActivity : AppCompatActivity() {
    private lateinit var user: User
    private lateinit var firstName: EditText
    private lateinit var lastName: EditText
    private lateinit var emailAddress: EditText
    private lateinit var btnSave: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user)
        init()
    }

    private fun init() {
        firstName = findViewById(R.id.etFirstName)
        lastName = findViewById(R.id.etLastName)
        emailAddress = findViewById(R.id.etEmailAddress)
        btnSave = findViewById(R.id.btnUserSave)
        btnSave.setOnClickListener(saveBtnClick)
//        intent.extras?.containsKey("position")
        if (intent.extras?.containsKey("user") == true) {
            user = intent.extras?.get("user") as User
            getHints(user)
        }

    }

    private fun isAllFieldsValid(): Boolean = !(emailAddress.text.isNullOrBlank() ||
            firstName.text.isNullOrBlank() ||
            lastName.text.isNullOrBlank())


    fun getHints(user: User) {
        firstName.hint = user.firstName
        lastName.hint = user.lastName
        emailAddress.hint = user.emailAddress
    }

    private val saveBtnClick = View.OnClickListener {
        if (isAllFieldsValid()) {
            val intent = intent
            if ((intent.extras?.get("user")) == null) {
                user = User(
                    firstName.text.toString(),
                    lastName.text.toString(),
                    emailAddress.text.toString()
                )
                val intent = intent
                intent.putExtra("user", user)
                setResult(RESULT_OK, intent)
                finish()
            }else{
                user.changeInfo(
                    firstName.text.toString(),
                    lastName.text.toString(),
                    emailAddress.text.toString()
                )
                val intent = intent
                intent.putExtra("user", user)
                intent.putExtra("position", intent.extras?.getInt("position"))
                setResult(RESULT_OK, intent)
                finish()
            }


        } else {
            btnSave.text = "Try Again!"
        }
    }
}

