package com.example.quiz_3_pra

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class User(var firstName: String, var lastName: String, var emailAddress: String): Parcelable{
    fun changeInfo(
        firstNameChanged: String,
        lastNameChanged: String,
        emailAddressChanged: String,
    ) {
        firstName = firstNameChanged
        lastName = lastNameChanged
        emailAddress = emailAddressChanged
    }



}

